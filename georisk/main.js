const menuIcon = document.querySelector('.menu__icon');
const menu = document.querySelector('.navigation__links');
const overlay = document.querySelector('.overlay');
const menuLinks = document.querySelectorAll('.navigation__link');
const src = ['images/menu.png','images/menu_close.png'];

function toggle() {
    menu.classList.toggle('navigation__links_visible');
    overlay.classList.toggle('overlay_visible');
    if (menuIcon.src.includes(src[0])) {
        menuIcon.src = src[1];
    }
    else menuIcon.src = src[0];
}

function toggler(item) {
    item.addEventListener('click', () => {
        toggle();
    })
}
for (let item of menuLinks) {
    toggler(item);
}
toggler(menuIcon);
toggler(overlay);
