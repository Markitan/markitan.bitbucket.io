import '../img/cat@1x.webp';
import '../img/cat@2x.webp';
import '../img/cat@1x.png';
import '../img/cat@2x.png';

export default function renderCardContent(data) {
  let elem = document.querySelector('.app');
  let defaultOffer = `Чего сидишь? Порадуй котэ, <span class="product-offer__submit">купи</span>`;
  let offer = '';

  if (~data.defaultState.indexOf('unselected')) {
    offer = defaultOffer;
  } else if (~data.defaultState.indexOf('disabled')){
    offer = `Печалька, ${data.content} закончился`;
  } else {
    offer = data.offer;
  }

  elem.innerHTML += `
  <div class="product-offer ${data.defaultState}">
          <div class="product-card">
            <div class="product-card__text">
                <h3 class="product-card__text_description">${data.description}</h3>
                <h1 class="product-card__text_title">${data.title}</h1>
                <h2 class="product-card__text_content">${data.content}</h2>
                <p class="product-card__text_count"><b>${data.count}</b> порций</p>
                <p class="product-card__text_prize">${!!data.prizeCount ? `<b>${data.prizeCount}</b>` : ''} ${data.prize}</p>
            </div>
            <picture>
                <source srcset="cat@1x.webp 1x,
                                cat@2x.webp 2x"
                        type="image/webp">
                <img src="cat@1x.png" alt="cat"
                     srcset="cat@2x.png 2x">
            </picture>
            <div class="product-card__weight">
              <span>${data.weight}</span><br>кг
            </div>
          </div>
          <div class="product-offer__text">
            ${offer}
          </div>
  </div>`;
}
