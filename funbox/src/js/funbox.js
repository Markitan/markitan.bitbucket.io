import {productData} from './dataExample';
import renderCardContent from './render';
import CardHandler from './cardHandler';

productData.forEach((data) => {
  renderCardContent(data);
});

let handler = new CardHandler();
handler.eventHandler();

