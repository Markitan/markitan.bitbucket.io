import {productData} from './dataExample';

export default class CardHandler {
  constructor() {
    this.offers = document.querySelectorAll('.product-offer');
    this.productDescription = document.querySelectorAll('.product-card__text_description');
    this.productOfferText = document.querySelectorAll('.product-offer__text');
    this.defaultOffer = `Чего сидишь? Порадуй котэ, <span class="product-offer__submit">купи</span>`;
  }
  selectReadyToHover() {
    this.classList.add('product-offer_selected-ready');
  }
  toggleSelect(item) {
    item.classList.toggle('product-offer_selected');
    item.classList.toggle('product-offer_unselected');
  }
  enableSelect(item, offerText, index){
    this.toggleSelect(item);
    item.addEventListener('mouseleave', this.selectReadyToHover);
    offerText.innerHTML = productData[index].offer;
  }

  eventHandler() {
    if (window.NodeList && !NodeList.prototype.forEach) {
      NodeList.prototype.forEach = Array.prototype.forEach;
    }

    this.offers.forEach((offer, index) => {
      let description = this.productDescription[index];
      let offerText = this.productOfferText[index];

      offer.addEventListener('click', (event) => {
        if (offer.classList.contains('product-offer_unselected') ||
          event.target.className === 'product-offer__submit'
        ) {
          this.enableSelect(offer, offerText, index);
        }
        else {
          this.toggleSelect(offer);
          offer.classList.remove('product-offer_selected-ready');
          offer.removeEventListener('mouseleave', this.selectReadyToHover);
          offerText.innerHTML = this.defaultOffer;
        }
        if (!offer.classList.contains('product-offer_selected-ready')) {
          description.innerText = productData[index].description;
        }
      });
      offer.addEventListener('mouseenter', () => {
        if (offer.classList.contains('product-offer_selected-ready')) {
          description.innerText = productData[index].warning;
        }
      });
      offer.addEventListener('mouseleave', () => {
        if (offer.classList.contains('product-offer_selected-ready')) {
          description.innerText = productData[index].description;
        }
      });

    })
  }
}
