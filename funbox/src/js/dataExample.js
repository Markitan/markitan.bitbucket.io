let productData = [
  {
    title:       'Нямушка',
    description: 'Сказочное заморское яство',
    content:     'с фуа-гра',
    count:       10,
    prize:       'мышь в подарок',
    offer:       'Печень утки разварная с артишоками',
    weight:      0.5,
    warning:     'Котэ не одобряет?',
    defaultState: 'product-offer_unselected'
  },
  {
    title:       'Нямушка',
    description: 'Сказочное заморское яство',
    content:     'с рыбой',
    count:       40,
    prizeCount:  2,
    prize:       'мыши в подарок',
    offer:       'Головы щучьи с чесноком да свежайшая сёмгушка',
    weight:      2,
    warning:     'Котэ не одобряет?',
    defaultState: 'product-offer_selected product-offer_selected-ready'
  },
  {
    title:       'Нямушка',
    description: 'Сказочное заморское яство',
    content:     'с курой',
    count:        100,
    prizeCount:   5,
    prize:       'мышей в подарок',
    details:     'заказчик доволен',
    offer:       'Филе из цыплят с трюфелями в бульон',
    weight:      7,
    warning:     'Котэ не одобряет?',
    defaultState: 'product-offer_disabled'
  }
];

export {productData};
