/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/js/app.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/img/cat@1x.png":
/*!****************************!*\
  !*** ./src/img/cat@1x.png ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "cat@1x.png";

/***/ }),

/***/ "./src/img/cat@1x.webp":
/*!*****************************!*\
  !*** ./src/img/cat@1x.webp ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "cat@1x.webp";

/***/ }),

/***/ "./src/img/cat@2x.png":
/*!****************************!*\
  !*** ./src/img/cat@2x.png ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "cat@2x.png";

/***/ }),

/***/ "./src/img/cat@2x.webp":
/*!*****************************!*\
  !*** ./src/img/cat@2x.webp ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "cat@2x.webp";

/***/ }),

/***/ "./src/js/app.js":
/*!***********************!*\
  !*** ./src/js/app.js ***!
  \***********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _funbox_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./funbox.js */ "./src/js/funbox.js");
/* harmony import */ var _dataExample_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./dataExample.js */ "./src/js/dataExample.js");
/* harmony import */ var _render_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./render.js */ "./src/js/render.js");
/* harmony import */ var _cardHandler_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./cardHandler.js */ "./src/js/cardHandler.js");
/* harmony import */ var _scss_funbox_scss__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../scss/funbox.scss */ "./src/scss/funbox.scss");
/* harmony import */ var _scss_funbox_scss__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_scss_funbox_scss__WEBPACK_IMPORTED_MODULE_4__);






/***/ }),

/***/ "./src/js/cardHandler.js":
/*!*******************************!*\
  !*** ./src/js/cardHandler.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CardHandler; });
/* harmony import */ var _dataExample__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./dataExample */ "./src/js/dataExample.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }



var CardHandler =
/*#__PURE__*/
function () {
  function CardHandler() {
    _classCallCheck(this, CardHandler);

    this.offers = document.querySelectorAll('.product-offer');
    this.productDescription = document.querySelectorAll('.product-card__text_description');
    this.productOfferText = document.querySelectorAll('.product-offer__text');
    this.defaultOffer = "\u0427\u0435\u0433\u043E \u0441\u0438\u0434\u0438\u0448\u044C? \u041F\u043E\u0440\u0430\u0434\u0443\u0439 \u043A\u043E\u0442\u044D, <span class=\"product-offer__submit\">\u043A\u0443\u043F\u0438</span>";
  }

  _createClass(CardHandler, [{
    key: "selectReadyToHover",
    value: function selectReadyToHover() {
      this.classList.add('product-offer_selected-ready');
    }
  }, {
    key: "toggleSelect",
    value: function toggleSelect(item) {
      item.classList.toggle('product-offer_selected');
      item.classList.toggle('product-offer_unselected');
    }
  }, {
    key: "enableSelect",
    value: function enableSelect(item, offerText, index) {
      this.toggleSelect(item);
      item.addEventListener('mouseleave', this.selectReadyToHover);
      offerText.innerHTML = _dataExample__WEBPACK_IMPORTED_MODULE_0__["productData"][index].offer;
    }
  }, {
    key: "eventHandler",
    value: function eventHandler() {
      var _this = this;

      if (window.NodeList && !NodeList.prototype.forEach) {
        NodeList.prototype.forEach = Array.prototype.forEach;
      }

      this.offers.forEach(function (offer, index) {
        var description = _this.productDescription[index];
        var offerText = _this.productOfferText[index];
        offer.addEventListener('click', function (event) {
          if (offer.classList.contains('product-offer_unselected') || event.target.className === 'product-offer__submit') {
            _this.enableSelect(offer, offerText, index);
          } else {
            _this.toggleSelect(offer);

            offer.classList.remove('product-offer_selected-ready');
            offer.removeEventListener('mouseleave', _this.selectReadyToHover);
            offerText.innerHTML = _this.defaultOffer;
          }

          if (!offer.classList.contains('product-offer_selected-ready')) {
            description.innerText = _dataExample__WEBPACK_IMPORTED_MODULE_0__["productData"][index].description;
          }
        });
        offer.addEventListener('mouseenter', function () {
          if (offer.classList.contains('product-offer_selected-ready')) {
            description.innerText = _dataExample__WEBPACK_IMPORTED_MODULE_0__["productData"][index].warning;
          }
        });
        offer.addEventListener('mouseleave', function () {
          if (offer.classList.contains('product-offer_selected-ready')) {
            description.innerText = _dataExample__WEBPACK_IMPORTED_MODULE_0__["productData"][index].description;
          }
        });
      });
    }
  }]);

  return CardHandler;
}();



/***/ }),

/***/ "./src/js/dataExample.js":
/*!*******************************!*\
  !*** ./src/js/dataExample.js ***!
  \*******************************/
/*! exports provided: productData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "productData", function() { return productData; });
var productData = [{
  title: 'Нямушка',
  description: 'Сказочное заморское яство',
  content: 'с фуа-гра',
  count: 10,
  prize: 'мышь в подарок',
  offer: 'Печень утки разварная с артишоками',
  weight: 0.5,
  warning: 'Котэ не одобряет?',
  defaultState: 'product-offer_unselected'
}, {
  title: 'Нямушка',
  description: 'Сказочное заморское яство',
  content: 'с рыбой',
  count: 40,
  prizeCount: 2,
  prize: 'мыши в подарок',
  offer: 'Головы щучьи с чесноком да свежайшая сёмгушка',
  weight: 2,
  warning: 'Котэ не одобряет?',
  defaultState: 'product-offer_selected product-offer_selected-ready'
}, {
  title: 'Нямушка',
  description: 'Сказочное заморское яство',
  content: 'с курой',
  count: 100,
  prizeCount: 5,
  prize: 'мышей в подарок',
  details: 'заказчик доволен',
  offer: 'Филе из цыплят с трюфелями в бульон',
  weight: 7,
  warning: 'Котэ не одобряет?',
  defaultState: 'product-offer_disabled'
}];


/***/ }),

/***/ "./src/js/funbox.js":
/*!**************************!*\
  !*** ./src/js/funbox.js ***!
  \**************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _dataExample__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./dataExample */ "./src/js/dataExample.js");
/* harmony import */ var _render__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./render */ "./src/js/render.js");
/* harmony import */ var _cardHandler__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./cardHandler */ "./src/js/cardHandler.js");



_dataExample__WEBPACK_IMPORTED_MODULE_0__["productData"].forEach(function (data) {
  Object(_render__WEBPACK_IMPORTED_MODULE_1__["default"])(data);
});
var handler = new _cardHandler__WEBPACK_IMPORTED_MODULE_2__["default"]();
handler.eventHandler();

/***/ }),

/***/ "./src/js/render.js":
/*!**************************!*\
  !*** ./src/js/render.js ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return renderCardContent; });
/* harmony import */ var _img_cat_1x_webp__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../img/cat@1x.webp */ "./src/img/cat@1x.webp");
/* harmony import */ var _img_cat_1x_webp__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_img_cat_1x_webp__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _img_cat_2x_webp__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../img/cat@2x.webp */ "./src/img/cat@2x.webp");
/* harmony import */ var _img_cat_2x_webp__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_img_cat_2x_webp__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _img_cat_1x_png__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../img/cat@1x.png */ "./src/img/cat@1x.png");
/* harmony import */ var _img_cat_1x_png__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_img_cat_1x_png__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _img_cat_2x_png__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../img/cat@2x.png */ "./src/img/cat@2x.png");
/* harmony import */ var _img_cat_2x_png__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_img_cat_2x_png__WEBPACK_IMPORTED_MODULE_3__);




function renderCardContent(data) {
  var elem = document.querySelector('.app');
  var defaultOffer = "\u0427\u0435\u0433\u043E \u0441\u0438\u0434\u0438\u0448\u044C? \u041F\u043E\u0440\u0430\u0434\u0443\u0439 \u043A\u043E\u0442\u044D, <span class=\"product-offer__submit\">\u043A\u0443\u043F\u0438</span>";
  var offer = '';

  if (~data.defaultState.indexOf('unselected')) {
    offer = defaultOffer;
  } else if (~data.defaultState.indexOf('disabled')) {
    offer = "\u041F\u0435\u0447\u0430\u043B\u044C\u043A\u0430, ".concat(data.content, " \u0437\u0430\u043A\u043E\u043D\u0447\u0438\u043B\u0441\u044F");
  } else {
    offer = data.offer;
  }

  elem.innerHTML += "\n  <div class=\"product-offer ".concat(data.defaultState, "\">\n          <div class=\"product-card\">\n            <div class=\"product-card__text\">\n                <h3 class=\"product-card__text_description\">").concat(data.description, "</h3>\n                <h1 class=\"product-card__text_title\">").concat(data.title, "</h1>\n                <h2 class=\"product-card__text_content\">").concat(data.content, "</h2>\n                <p class=\"product-card__text_count\"><b>").concat(data.count, "</b> \u043F\u043E\u0440\u0446\u0438\u0439</p>\n                <p class=\"product-card__text_prize\">").concat(!!data.prizeCount ? "<b>".concat(data.prizeCount, "</b>") : '', " ").concat(data.prize, "</p>\n            </div>\n            <picture>\n                <source srcset=\"cat@1x.webp 1x,\n                                cat@2x.webp 2x\"\n                        type=\"image/webp\">\n                <img src=\"cat@1x.png\" alt=\"cat\"\n                     srcset=\"cat@2x.png 2x\">\n            </picture>\n            <div class=\"product-card__weight\">\n              <span>").concat(data.weight, "</span><br>\u043A\u0433\n            </div>\n          </div>\n          <div class=\"product-offer__text\">\n            ").concat(offer, "\n          </div>\n  </div>");
}

/***/ }),

/***/ "./src/scss/funbox.scss":
/*!******************************!*\
  !*** ./src/scss/funbox.scss ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2ltZy9jYXRAMXgucG5nIiwid2VicGFjazovLy8uL3NyYy9pbWcvY2F0QDF4LndlYnAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2ltZy9jYXRAMngucG5nIiwid2VicGFjazovLy8uL3NyYy9pbWcvY2F0QDJ4LndlYnAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2pzL2FwcC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvanMvY2FyZEhhbmRsZXIuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2pzL2RhdGFFeGFtcGxlLmpzIiwid2VicGFjazovLy8uL3NyYy9qcy9mdW5ib3guanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2pzL3JlbmRlci5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc2Nzcy9mdW5ib3guc2NzcyJdLCJuYW1lcyI6WyJDYXJkSGFuZGxlciIsIm9mZmVycyIsImRvY3VtZW50IiwicXVlcnlTZWxlY3RvckFsbCIsInByb2R1Y3REZXNjcmlwdGlvbiIsInByb2R1Y3RPZmZlclRleHQiLCJkZWZhdWx0T2ZmZXIiLCJjbGFzc0xpc3QiLCJhZGQiLCJpdGVtIiwidG9nZ2xlIiwib2ZmZXJUZXh0IiwiaW5kZXgiLCJ0b2dnbGVTZWxlY3QiLCJhZGRFdmVudExpc3RlbmVyIiwic2VsZWN0UmVhZHlUb0hvdmVyIiwiaW5uZXJIVE1MIiwicHJvZHVjdERhdGEiLCJvZmZlciIsIndpbmRvdyIsIk5vZGVMaXN0IiwicHJvdG90eXBlIiwiZm9yRWFjaCIsIkFycmF5IiwiZGVzY3JpcHRpb24iLCJldmVudCIsImNvbnRhaW5zIiwidGFyZ2V0IiwiY2xhc3NOYW1lIiwiZW5hYmxlU2VsZWN0IiwicmVtb3ZlIiwicmVtb3ZlRXZlbnRMaXN0ZW5lciIsImlubmVyVGV4dCIsIndhcm5pbmciLCJ0aXRsZSIsImNvbnRlbnQiLCJjb3VudCIsInByaXplIiwid2VpZ2h0IiwiZGVmYXVsdFN0YXRlIiwicHJpemVDb3VudCIsImRldGFpbHMiLCJkYXRhIiwicmVuZGVyQ2FyZENvbnRlbnQiLCJoYW5kbGVyIiwiZXZlbnRIYW5kbGVyIiwiZWxlbSIsInF1ZXJ5U2VsZWN0b3IiLCJpbmRleE9mIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxrREFBMEMsZ0NBQWdDO0FBQzFFO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsZ0VBQXdELGtCQUFrQjtBQUMxRTtBQUNBLHlEQUFpRCxjQUFjO0FBQy9EOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpREFBeUMsaUNBQWlDO0FBQzFFLHdIQUFnSCxtQkFBbUIsRUFBRTtBQUNySTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOzs7QUFHQTtBQUNBOzs7Ozs7Ozs7Ozs7QUNsRkEsaUJBQWlCLHFCQUF1QixnQjs7Ozs7Ozs7Ozs7QUNBeEMsaUJBQWlCLHFCQUF1QixpQjs7Ozs7Ozs7Ozs7QUNBeEMsaUJBQWlCLHFCQUF1QixnQjs7Ozs7Ozs7Ozs7QUNBeEMsaUJBQWlCLHFCQUF1QixpQjs7Ozs7Ozs7Ozs7O0FDQXhDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNIQTs7SUFFcUJBLFc7OztBQUNuQix5QkFBYztBQUFBOztBQUNaLFNBQUtDLE1BQUwsR0FBY0MsUUFBUSxDQUFDQyxnQkFBVCxDQUEwQixnQkFBMUIsQ0FBZDtBQUNBLFNBQUtDLGtCQUFMLEdBQTBCRixRQUFRLENBQUNDLGdCQUFULENBQTBCLGlDQUExQixDQUExQjtBQUNBLFNBQUtFLGdCQUFMLEdBQXdCSCxRQUFRLENBQUNDLGdCQUFULENBQTBCLHNCQUExQixDQUF4QjtBQUNBLFNBQUtHLFlBQUw7QUFDRDs7Ozt5Q0FDb0I7QUFDbkIsV0FBS0MsU0FBTCxDQUFlQyxHQUFmLENBQW1CLDhCQUFuQjtBQUNEOzs7aUNBQ1lDLEksRUFBTTtBQUNqQkEsVUFBSSxDQUFDRixTQUFMLENBQWVHLE1BQWYsQ0FBc0Isd0JBQXRCO0FBQ0FELFVBQUksQ0FBQ0YsU0FBTCxDQUFlRyxNQUFmLENBQXNCLDBCQUF0QjtBQUNEOzs7aUNBQ1lELEksRUFBTUUsUyxFQUFXQyxLLEVBQU07QUFDbEMsV0FBS0MsWUFBTCxDQUFrQkosSUFBbEI7QUFDQUEsVUFBSSxDQUFDSyxnQkFBTCxDQUFzQixZQUF0QixFQUFvQyxLQUFLQyxrQkFBekM7QUFDQUosZUFBUyxDQUFDSyxTQUFWLEdBQXNCQyx3REFBVyxDQUFDTCxLQUFELENBQVgsQ0FBbUJNLEtBQXpDO0FBQ0Q7OzttQ0FFYztBQUFBOztBQUNiLFVBQUlDLE1BQU0sQ0FBQ0MsUUFBUCxJQUFtQixDQUFDQSxRQUFRLENBQUNDLFNBQVQsQ0FBbUJDLE9BQTNDLEVBQW9EO0FBQ2xERixnQkFBUSxDQUFDQyxTQUFULENBQW1CQyxPQUFuQixHQUE2QkMsS0FBSyxDQUFDRixTQUFOLENBQWdCQyxPQUE3QztBQUNEOztBQUVELFdBQUtyQixNQUFMLENBQVlxQixPQUFaLENBQW9CLFVBQUNKLEtBQUQsRUFBUU4sS0FBUixFQUFrQjtBQUNwQyxZQUFJWSxXQUFXLEdBQUcsS0FBSSxDQUFDcEIsa0JBQUwsQ0FBd0JRLEtBQXhCLENBQWxCO0FBQ0EsWUFBSUQsU0FBUyxHQUFHLEtBQUksQ0FBQ04sZ0JBQUwsQ0FBc0JPLEtBQXRCLENBQWhCO0FBRUFNLGFBQUssQ0FBQ0osZ0JBQU4sQ0FBdUIsT0FBdkIsRUFBZ0MsVUFBQ1csS0FBRCxFQUFXO0FBQ3pDLGNBQUlQLEtBQUssQ0FBQ1gsU0FBTixDQUFnQm1CLFFBQWhCLENBQXlCLDBCQUF6QixLQUNGRCxLQUFLLENBQUNFLE1BQU4sQ0FBYUMsU0FBYixLQUEyQix1QkFEN0IsRUFFRTtBQUNBLGlCQUFJLENBQUNDLFlBQUwsQ0FBa0JYLEtBQWxCLEVBQXlCUCxTQUF6QixFQUFvQ0MsS0FBcEM7QUFDRCxXQUpELE1BS0s7QUFDSCxpQkFBSSxDQUFDQyxZQUFMLENBQWtCSyxLQUFsQjs7QUFDQUEsaUJBQUssQ0FBQ1gsU0FBTixDQUFnQnVCLE1BQWhCLENBQXVCLDhCQUF2QjtBQUNBWixpQkFBSyxDQUFDYSxtQkFBTixDQUEwQixZQUExQixFQUF3QyxLQUFJLENBQUNoQixrQkFBN0M7QUFDQUoscUJBQVMsQ0FBQ0ssU0FBVixHQUFzQixLQUFJLENBQUNWLFlBQTNCO0FBQ0Q7O0FBQ0QsY0FBSSxDQUFDWSxLQUFLLENBQUNYLFNBQU4sQ0FBZ0JtQixRQUFoQixDQUF5Qiw4QkFBekIsQ0FBTCxFQUErRDtBQUM3REYsdUJBQVcsQ0FBQ1EsU0FBWixHQUF3QmYsd0RBQVcsQ0FBQ0wsS0FBRCxDQUFYLENBQW1CWSxXQUEzQztBQUNEO0FBQ0YsU0FmRDtBQWdCQU4sYUFBSyxDQUFDSixnQkFBTixDQUF1QixZQUF2QixFQUFxQyxZQUFNO0FBQ3pDLGNBQUlJLEtBQUssQ0FBQ1gsU0FBTixDQUFnQm1CLFFBQWhCLENBQXlCLDhCQUF6QixDQUFKLEVBQThEO0FBQzVERix1QkFBVyxDQUFDUSxTQUFaLEdBQXdCZix3REFBVyxDQUFDTCxLQUFELENBQVgsQ0FBbUJxQixPQUEzQztBQUNEO0FBQ0YsU0FKRDtBQUtBZixhQUFLLENBQUNKLGdCQUFOLENBQXVCLFlBQXZCLEVBQXFDLFlBQU07QUFDekMsY0FBSUksS0FBSyxDQUFDWCxTQUFOLENBQWdCbUIsUUFBaEIsQ0FBeUIsOEJBQXpCLENBQUosRUFBOEQ7QUFDNURGLHVCQUFXLENBQUNRLFNBQVosR0FBd0JmLHdEQUFXLENBQUNMLEtBQUQsQ0FBWCxDQUFtQlksV0FBM0M7QUFDRDtBQUNGLFNBSkQ7QUFNRCxPQS9CRDtBQWdDRDs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDM0RIO0FBQUE7QUFBQSxJQUFJUCxXQUFXLEdBQUcsQ0FDaEI7QUFDRWlCLE9BQUssRUFBUSxTQURmO0FBRUVWLGFBQVcsRUFBRSwyQkFGZjtBQUdFVyxTQUFPLEVBQU0sV0FIZjtBQUlFQyxPQUFLLEVBQVEsRUFKZjtBQUtFQyxPQUFLLEVBQVEsZ0JBTGY7QUFNRW5CLE9BQUssRUFBUSxvQ0FOZjtBQU9Fb0IsUUFBTSxFQUFPLEdBUGY7QUFRRUwsU0FBTyxFQUFNLG1CQVJmO0FBU0VNLGNBQVksRUFBRTtBQVRoQixDQURnQixFQVloQjtBQUNFTCxPQUFLLEVBQVEsU0FEZjtBQUVFVixhQUFXLEVBQUUsMkJBRmY7QUFHRVcsU0FBTyxFQUFNLFNBSGY7QUFJRUMsT0FBSyxFQUFRLEVBSmY7QUFLRUksWUFBVSxFQUFHLENBTGY7QUFNRUgsT0FBSyxFQUFRLGdCQU5mO0FBT0VuQixPQUFLLEVBQVEsK0NBUGY7QUFRRW9CLFFBQU0sRUFBTyxDQVJmO0FBU0VMLFNBQU8sRUFBTSxtQkFUZjtBQVVFTSxjQUFZLEVBQUU7QUFWaEIsQ0FaZ0IsRUF3QmhCO0FBQ0VMLE9BQUssRUFBUSxTQURmO0FBRUVWLGFBQVcsRUFBRSwyQkFGZjtBQUdFVyxTQUFPLEVBQU0sU0FIZjtBQUlFQyxPQUFLLEVBQVMsR0FKaEI7QUFLRUksWUFBVSxFQUFJLENBTGhCO0FBTUVILE9BQUssRUFBUSxpQkFOZjtBQU9FSSxTQUFPLEVBQU0sa0JBUGY7QUFRRXZCLE9BQUssRUFBUSxxQ0FSZjtBQVNFb0IsUUFBTSxFQUFPLENBVGY7QUFVRUwsU0FBTyxFQUFNLG1CQVZmO0FBV0VNLGNBQVksRUFBRTtBQVhoQixDQXhCZ0IsQ0FBbEI7Ozs7Ozs7Ozs7Ozs7QUNBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUVBdEIsd0RBQVcsQ0FBQ0ssT0FBWixDQUFvQixVQUFDb0IsSUFBRCxFQUFVO0FBQzVCQyx5REFBaUIsQ0FBQ0QsSUFBRCxDQUFqQjtBQUNELENBRkQ7QUFJQSxJQUFJRSxPQUFPLEdBQUcsSUFBSTVDLG9EQUFKLEVBQWQ7QUFDQTRDLE9BQU8sQ0FBQ0MsWUFBUixHOzs7Ozs7Ozs7Ozs7QUNUQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRWUsU0FBU0YsaUJBQVQsQ0FBMkJELElBQTNCLEVBQWlDO0FBQzlDLE1BQUlJLElBQUksR0FBRzVDLFFBQVEsQ0FBQzZDLGFBQVQsQ0FBdUIsTUFBdkIsQ0FBWDtBQUNBLE1BQUl6QyxZQUFZLDhNQUFoQjtBQUNBLE1BQUlZLEtBQUssR0FBRyxFQUFaOztBQUVBLE1BQUksQ0FBQ3dCLElBQUksQ0FBQ0gsWUFBTCxDQUFrQlMsT0FBbEIsQ0FBMEIsWUFBMUIsQ0FBTCxFQUE4QztBQUM1QzlCLFNBQUssR0FBR1osWUFBUjtBQUNELEdBRkQsTUFFTyxJQUFJLENBQUNvQyxJQUFJLENBQUNILFlBQUwsQ0FBa0JTLE9BQWxCLENBQTBCLFVBQTFCLENBQUwsRUFBMkM7QUFDaEQ5QixTQUFLLCtEQUFnQndCLElBQUksQ0FBQ1AsT0FBckIsa0VBQUw7QUFDRCxHQUZNLE1BRUE7QUFDTGpCLFNBQUssR0FBR3dCLElBQUksQ0FBQ3hCLEtBQWI7QUFDRDs7QUFFRDRCLE1BQUksQ0FBQzlCLFNBQUwsNkNBQzRCMEIsSUFBSSxDQUFDSCxZQURqQyx1S0FJMkRHLElBQUksQ0FBQ2xCLFdBSmhFLDJFQUtxRGtCLElBQUksQ0FBQ1IsS0FMMUQsNkVBTXVEUSxJQUFJLENBQUNQLE9BTjVELDZFQU91RE8sSUFBSSxDQUFDTixLQVA1RCxrSEFRb0QsQ0FBQyxDQUFDTSxJQUFJLENBQUNGLFVBQVAsZ0JBQTBCRSxJQUFJLENBQUNGLFVBQS9CLFlBQWtELEVBUnRHLGNBUTRHRSxJQUFJLENBQUNMLEtBUmpILG1aQWtCb0JLLElBQUksQ0FBQ0osTUFsQnpCLHVJQXNCWXBCLEtBdEJaO0FBeUJELEM7Ozs7Ozs7Ozs7O0FDM0NELHlDIiwiZmlsZSI6ImJ1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc3JjL2pzL2FwcC5qc1wiKTtcbiIsIm1vZHVsZS5leHBvcnRzID0gX193ZWJwYWNrX3B1YmxpY19wYXRoX18gKyBcImNhdEAxeC5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IF9fd2VicGFja19wdWJsaWNfcGF0aF9fICsgXCJjYXRAMXgud2VicFwiOyIsIm1vZHVsZS5leHBvcnRzID0gX193ZWJwYWNrX3B1YmxpY19wYXRoX18gKyBcImNhdEAyeC5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IF9fd2VicGFja19wdWJsaWNfcGF0aF9fICsgXCJjYXRAMngud2VicFwiOyIsImltcG9ydCAnLi9mdW5ib3guanMnO1xuaW1wb3J0ICcuL2RhdGFFeGFtcGxlLmpzJztcbmltcG9ydCAnLi9yZW5kZXIuanMnO1xuaW1wb3J0ICcuL2NhcmRIYW5kbGVyLmpzJztcbmltcG9ydCAnLi4vc2Nzcy9mdW5ib3guc2Nzcyc7XG5cbiIsImltcG9ydCB7cHJvZHVjdERhdGF9IGZyb20gJy4vZGF0YUV4YW1wbGUnO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBDYXJkSGFuZGxlciB7XG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMub2ZmZXJzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLnByb2R1Y3Qtb2ZmZXInKTtcbiAgICB0aGlzLnByb2R1Y3REZXNjcmlwdGlvbiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5wcm9kdWN0LWNhcmRfX3RleHRfZGVzY3JpcHRpb24nKTtcbiAgICB0aGlzLnByb2R1Y3RPZmZlclRleHQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcucHJvZHVjdC1vZmZlcl9fdGV4dCcpO1xuICAgIHRoaXMuZGVmYXVsdE9mZmVyID0gYNCn0LXQs9C+INGB0LjQtNC40YjRjD8g0J/QvtGA0LDQtNGD0Lkg0LrQvtGC0Y0sIDxzcGFuIGNsYXNzPVwicHJvZHVjdC1vZmZlcl9fc3VibWl0XCI+0LrRg9C/0Lg8L3NwYW4+YDtcbiAgfVxuICBzZWxlY3RSZWFkeVRvSG92ZXIoKSB7XG4gICAgdGhpcy5jbGFzc0xpc3QuYWRkKCdwcm9kdWN0LW9mZmVyX3NlbGVjdGVkLXJlYWR5Jyk7XG4gIH1cbiAgdG9nZ2xlU2VsZWN0KGl0ZW0pIHtcbiAgICBpdGVtLmNsYXNzTGlzdC50b2dnbGUoJ3Byb2R1Y3Qtb2ZmZXJfc2VsZWN0ZWQnKTtcbiAgICBpdGVtLmNsYXNzTGlzdC50b2dnbGUoJ3Byb2R1Y3Qtb2ZmZXJfdW5zZWxlY3RlZCcpO1xuICB9XG4gIGVuYWJsZVNlbGVjdChpdGVtLCBvZmZlclRleHQsIGluZGV4KXtcbiAgICB0aGlzLnRvZ2dsZVNlbGVjdChpdGVtKTtcbiAgICBpdGVtLmFkZEV2ZW50TGlzdGVuZXIoJ21vdXNlbGVhdmUnLCB0aGlzLnNlbGVjdFJlYWR5VG9Ib3Zlcik7XG4gICAgb2ZmZXJUZXh0LmlubmVySFRNTCA9IHByb2R1Y3REYXRhW2luZGV4XS5vZmZlcjtcbiAgfVxuXG4gIGV2ZW50SGFuZGxlcigpIHtcbiAgICBpZiAod2luZG93Lk5vZGVMaXN0ICYmICFOb2RlTGlzdC5wcm90b3R5cGUuZm9yRWFjaCkge1xuICAgICAgTm9kZUxpc3QucHJvdG90eXBlLmZvckVhY2ggPSBBcnJheS5wcm90b3R5cGUuZm9yRWFjaDtcbiAgICB9XG5cbiAgICB0aGlzLm9mZmVycy5mb3JFYWNoKChvZmZlciwgaW5kZXgpID0+IHtcbiAgICAgIGxldCBkZXNjcmlwdGlvbiA9IHRoaXMucHJvZHVjdERlc2NyaXB0aW9uW2luZGV4XTtcbiAgICAgIGxldCBvZmZlclRleHQgPSB0aGlzLnByb2R1Y3RPZmZlclRleHRbaW5kZXhdO1xuXG4gICAgICBvZmZlci5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIChldmVudCkgPT4ge1xuICAgICAgICBpZiAob2ZmZXIuY2xhc3NMaXN0LmNvbnRhaW5zKCdwcm9kdWN0LW9mZmVyX3Vuc2VsZWN0ZWQnKSB8fFxuICAgICAgICAgIGV2ZW50LnRhcmdldC5jbGFzc05hbWUgPT09ICdwcm9kdWN0LW9mZmVyX19zdWJtaXQnXG4gICAgICAgICkge1xuICAgICAgICAgIHRoaXMuZW5hYmxlU2VsZWN0KG9mZmVyLCBvZmZlclRleHQsIGluZGV4KTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICB0aGlzLnRvZ2dsZVNlbGVjdChvZmZlcik7XG4gICAgICAgICAgb2ZmZXIuY2xhc3NMaXN0LnJlbW92ZSgncHJvZHVjdC1vZmZlcl9zZWxlY3RlZC1yZWFkeScpO1xuICAgICAgICAgIG9mZmVyLnJlbW92ZUV2ZW50TGlzdGVuZXIoJ21vdXNlbGVhdmUnLCB0aGlzLnNlbGVjdFJlYWR5VG9Ib3Zlcik7XG4gICAgICAgICAgb2ZmZXJUZXh0LmlubmVySFRNTCA9IHRoaXMuZGVmYXVsdE9mZmVyO1xuICAgICAgICB9XG4gICAgICAgIGlmICghb2ZmZXIuY2xhc3NMaXN0LmNvbnRhaW5zKCdwcm9kdWN0LW9mZmVyX3NlbGVjdGVkLXJlYWR5JykpIHtcbiAgICAgICAgICBkZXNjcmlwdGlvbi5pbm5lclRleHQgPSBwcm9kdWN0RGF0YVtpbmRleF0uZGVzY3JpcHRpb247XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgICAgb2ZmZXIuYWRkRXZlbnRMaXN0ZW5lcignbW91c2VlbnRlcicsICgpID0+IHtcbiAgICAgICAgaWYgKG9mZmVyLmNsYXNzTGlzdC5jb250YWlucygncHJvZHVjdC1vZmZlcl9zZWxlY3RlZC1yZWFkeScpKSB7XG4gICAgICAgICAgZGVzY3JpcHRpb24uaW5uZXJUZXh0ID0gcHJvZHVjdERhdGFbaW5kZXhdLndhcm5pbmc7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgICAgb2ZmZXIuYWRkRXZlbnRMaXN0ZW5lcignbW91c2VsZWF2ZScsICgpID0+IHtcbiAgICAgICAgaWYgKG9mZmVyLmNsYXNzTGlzdC5jb250YWlucygncHJvZHVjdC1vZmZlcl9zZWxlY3RlZC1yZWFkeScpKSB7XG4gICAgICAgICAgZGVzY3JpcHRpb24uaW5uZXJUZXh0ID0gcHJvZHVjdERhdGFbaW5kZXhdLmRlc2NyaXB0aW9uO1xuICAgICAgICB9XG4gICAgICB9KTtcblxuICAgIH0pXG4gIH1cbn1cbiIsImxldCBwcm9kdWN0RGF0YSA9IFtcbiAge1xuICAgIHRpdGxlOiAgICAgICAn0J3Rj9C80YPRiNC60LAnLFxuICAgIGRlc2NyaXB0aW9uOiAn0KHQutCw0LfQvtGH0L3QvtC1INC30LDQvNC+0YDRgdC60L7QtSDRj9GB0YLQstC+JyxcbiAgICBjb250ZW50OiAgICAgJ9GBINGE0YPQsC3Qs9GA0LAnLFxuICAgIGNvdW50OiAgICAgICAxMCxcbiAgICBwcml6ZTogICAgICAgJ9C80YvRiNGMINCyINC/0L7QtNCw0YDQvtC6JyxcbiAgICBvZmZlcjogICAgICAgJ9Cf0LXRh9C10L3RjCDRg9GC0LrQuCDRgNCw0LfQstCw0YDQvdCw0Y8g0YEg0LDRgNGC0LjRiNC+0LrQsNC80LgnLFxuICAgIHdlaWdodDogICAgICAwLjUsXG4gICAgd2FybmluZzogICAgICfQmtC+0YLRjSDQvdC1INC+0LTQvtCx0YDRj9C10YI/JyxcbiAgICBkZWZhdWx0U3RhdGU6ICdwcm9kdWN0LW9mZmVyX3Vuc2VsZWN0ZWQnXG4gIH0sXG4gIHtcbiAgICB0aXRsZTogICAgICAgJ9Cd0Y/QvNGD0YjQutCwJyxcbiAgICBkZXNjcmlwdGlvbjogJ9Ch0LrQsNC30L7Rh9C90L7QtSDQt9Cw0LzQvtGA0YHQutC+0LUg0Y/RgdGC0LLQvicsXG4gICAgY29udGVudDogICAgICfRgSDRgNGL0LHQvtC5JyxcbiAgICBjb3VudDogICAgICAgNDAsXG4gICAgcHJpemVDb3VudDogIDIsXG4gICAgcHJpemU6ICAgICAgICfQvNGL0YjQuCDQsiDQv9C+0LTQsNGA0L7QuicsXG4gICAgb2ZmZXI6ICAgICAgICfQk9C+0LvQvtCy0Ysg0YnRg9GH0YzQuCDRgSDRh9C10YHQvdC+0LrQvtC8INC00LAg0YHQstC10LbQsNC50YjQsNGPINGB0ZHQvNCz0YPRiNC60LAnLFxuICAgIHdlaWdodDogICAgICAyLFxuICAgIHdhcm5pbmc6ICAgICAn0JrQvtGC0Y0g0L3QtSDQvtC00L7QsdGA0Y/QtdGCPycsXG4gICAgZGVmYXVsdFN0YXRlOiAncHJvZHVjdC1vZmZlcl9zZWxlY3RlZCBwcm9kdWN0LW9mZmVyX3NlbGVjdGVkLXJlYWR5J1xuICB9LFxuICB7XG4gICAgdGl0bGU6ICAgICAgICfQndGP0LzRg9GI0LrQsCcsXG4gICAgZGVzY3JpcHRpb246ICfQodC60LDQt9C+0YfQvdC+0LUg0LfQsNC80L7RgNGB0LrQvtC1INGP0YHRgtCy0L4nLFxuICAgIGNvbnRlbnQ6ICAgICAn0YEg0LrRg9GA0L7QuScsXG4gICAgY291bnQ6ICAgICAgICAxMDAsXG4gICAgcHJpemVDb3VudDogICA1LFxuICAgIHByaXplOiAgICAgICAn0LzRi9GI0LXQuSDQsiDQv9C+0LTQsNGA0L7QuicsXG4gICAgZGV0YWlsczogICAgICfQt9Cw0LrQsNC30YfQuNC6INC00L7QstC+0LvQtdC9JyxcbiAgICBvZmZlcjogICAgICAgJ9Ck0LjQu9C1INC40Lcg0YbRi9C/0LvRj9GCINGBINGC0YDRjtGE0LXQu9GP0LzQuCDQsiDQsdGD0LvRjNC+0L0nLFxuICAgIHdlaWdodDogICAgICA3LFxuICAgIHdhcm5pbmc6ICAgICAn0JrQvtGC0Y0g0L3QtSDQvtC00L7QsdGA0Y/QtdGCPycsXG4gICAgZGVmYXVsdFN0YXRlOiAncHJvZHVjdC1vZmZlcl9kaXNhYmxlZCdcbiAgfVxuXTtcblxuZXhwb3J0IHtwcm9kdWN0RGF0YX07XG4iLCJpbXBvcnQge3Byb2R1Y3REYXRhfSBmcm9tICcuL2RhdGFFeGFtcGxlJztcbmltcG9ydCByZW5kZXJDYXJkQ29udGVudCBmcm9tICcuL3JlbmRlcic7XG5pbXBvcnQgQ2FyZEhhbmRsZXIgZnJvbSAnLi9jYXJkSGFuZGxlcic7XG5cbnByb2R1Y3REYXRhLmZvckVhY2goKGRhdGEpID0+IHtcbiAgcmVuZGVyQ2FyZENvbnRlbnQoZGF0YSk7XG59KTtcblxubGV0IGhhbmRsZXIgPSBuZXcgQ2FyZEhhbmRsZXIoKTtcbmhhbmRsZXIuZXZlbnRIYW5kbGVyKCk7XG5cbiIsImltcG9ydCAnLi4vaW1nL2NhdEAxeC53ZWJwJztcbmltcG9ydCAnLi4vaW1nL2NhdEAyeC53ZWJwJztcbmltcG9ydCAnLi4vaW1nL2NhdEAxeC5wbmcnO1xuaW1wb3J0ICcuLi9pbWcvY2F0QDJ4LnBuZyc7XG5cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIHJlbmRlckNhcmRDb250ZW50KGRhdGEpIHtcbiAgbGV0IGVsZW0gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuYXBwJyk7XG4gIGxldCBkZWZhdWx0T2ZmZXIgPSBg0KfQtdCz0L4g0YHQuNC00LjRiNGMPyDQn9C+0YDQsNC00YPQuSDQutC+0YLRjSwgPHNwYW4gY2xhc3M9XCJwcm9kdWN0LW9mZmVyX19zdWJtaXRcIj7QutGD0L/QuDwvc3Bhbj5gO1xuICBsZXQgb2ZmZXIgPSAnJztcblxuICBpZiAofmRhdGEuZGVmYXVsdFN0YXRlLmluZGV4T2YoJ3Vuc2VsZWN0ZWQnKSkge1xuICAgIG9mZmVyID0gZGVmYXVsdE9mZmVyO1xuICB9IGVsc2UgaWYgKH5kYXRhLmRlZmF1bHRTdGF0ZS5pbmRleE9mKCdkaXNhYmxlZCcpKXtcbiAgICBvZmZlciA9IGDQn9C10YfQsNC70YzQutCwLCAke2RhdGEuY29udGVudH0g0LfQsNC60L7QvdGH0LjQu9GB0Y9gO1xuICB9IGVsc2Uge1xuICAgIG9mZmVyID0gZGF0YS5vZmZlcjtcbiAgfVxuXG4gIGVsZW0uaW5uZXJIVE1MICs9IGBcbiAgPGRpdiBjbGFzcz1cInByb2R1Y3Qtb2ZmZXIgJHtkYXRhLmRlZmF1bHRTdGF0ZX1cIj5cbiAgICAgICAgICA8ZGl2IGNsYXNzPVwicHJvZHVjdC1jYXJkXCI+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwicHJvZHVjdC1jYXJkX190ZXh0XCI+XG4gICAgICAgICAgICAgICAgPGgzIGNsYXNzPVwicHJvZHVjdC1jYXJkX190ZXh0X2Rlc2NyaXB0aW9uXCI+JHtkYXRhLmRlc2NyaXB0aW9ufTwvaDM+XG4gICAgICAgICAgICAgICAgPGgxIGNsYXNzPVwicHJvZHVjdC1jYXJkX190ZXh0X3RpdGxlXCI+JHtkYXRhLnRpdGxlfTwvaDE+XG4gICAgICAgICAgICAgICAgPGgyIGNsYXNzPVwicHJvZHVjdC1jYXJkX190ZXh0X2NvbnRlbnRcIj4ke2RhdGEuY29udGVudH08L2gyPlxuICAgICAgICAgICAgICAgIDxwIGNsYXNzPVwicHJvZHVjdC1jYXJkX190ZXh0X2NvdW50XCI+PGI+JHtkYXRhLmNvdW50fTwvYj4g0L/QvtGA0YbQuNC5PC9wPlxuICAgICAgICAgICAgICAgIDxwIGNsYXNzPVwicHJvZHVjdC1jYXJkX190ZXh0X3ByaXplXCI+JHshIWRhdGEucHJpemVDb3VudCA/IGA8Yj4ke2RhdGEucHJpemVDb3VudH08L2I+YCA6ICcnfSAke2RhdGEucHJpemV9PC9wPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8cGljdHVyZT5cbiAgICAgICAgICAgICAgICA8c291cmNlIHNyY3NldD1cImNhdEAxeC53ZWJwIDF4LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYXRAMngud2VicCAyeFwiXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlPVwiaW1hZ2Uvd2VicFwiPlxuICAgICAgICAgICAgICAgIDxpbWcgc3JjPVwiY2F0QDF4LnBuZ1wiIGFsdD1cImNhdFwiXG4gICAgICAgICAgICAgICAgICAgICBzcmNzZXQ9XCJjYXRAMngucG5nIDJ4XCI+XG4gICAgICAgICAgICA8L3BpY3R1cmU+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwicHJvZHVjdC1jYXJkX193ZWlnaHRcIj5cbiAgICAgICAgICAgICAgPHNwYW4+JHtkYXRhLndlaWdodH08L3NwYW4+PGJyPtC60LNcbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJwcm9kdWN0LW9mZmVyX190ZXh0XCI+XG4gICAgICAgICAgICAke29mZmVyfVxuICAgICAgICAgIDwvZGl2PlxuICA8L2Rpdj5gO1xufVxuIiwiLy8gcmVtb3ZlZCBieSBleHRyYWN0LXRleHQtd2VicGFjay1wbHVnaW4iXSwic291cmNlUm9vdCI6IiJ9