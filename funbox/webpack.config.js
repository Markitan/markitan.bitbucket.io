const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode:      'development',
  entry:     './src/js/app.js',
  devtool:   'inline-source-map',
  devServer: {
    contentBase: './dist'
  },
  output: {
    path:     path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use:  ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use:      ['css-loader', 'sass-loader']
        })
      },
      {
        test:    /\.(png|svg|jpg|gif|webp)$/,
        use:     [
          {
            loader:  'file-loader',
            options: {
              name:       '[name].[ext]'
            }
          }
        ]
      },
      {
        test:    /\.js$/,
        exclude: /node_modules/,
        use:     {
          loader: 'babel-loader'
        }
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin('style.css'),
    new HtmlWebpackPlugin({
      title:    'Development',
      template: './index.html'
    })
  ]
};
